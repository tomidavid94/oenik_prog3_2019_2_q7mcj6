﻿// <copyright file="StocksRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Repository
{
    using System;
    using System.Linq;
    using System.Threading;
    using OrderPlacement.Data;
    using OrderPlacement.Repository.Interfaces;

    /// <summary>
    /// Repository for accessing Stocks
    /// </summary>
    public class StocksRepository : Repository, IStocksRepository
    {
        private OrderPlacementTablesEntities dbEntity = new OrderPlacementTablesEntities();

        /// <summary>
        /// Changes an entity in the table
        /// </summary>
        /// <param name="entity">ID of entity</param>
        /// <param name="field">Number of field</param>
        /// <param name="newfield">Value of field's new value</param>
        public void Change(Stocks entity, string field, string newfield)
        {
            int asd = 0;
            try
            {
                switch (field)
                {
                    case "ExpectedShipTimeInDays":
                        try
                        {
                            asd = int.Parse(newfield);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            return;
                        }

                        entity.ExpectedShipTimeInDays = asd;
                        break;
                    case "ItemDescription":
                        entity.ItemDescription = newfield;
                        break;
                    case "StockAmount":
                        try
                        {
                            asd = int.Parse(newfield);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            return;
                        }

                        entity.StockAmount = asd;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            this.dbEntity.SaveChanges();
        }

        /// <summary>
        /// Returns all stocks as queryable
        /// </summary>
        /// <returns>All stocks</returns>
        public IQueryable<Stocks> GetAll()
        {
            return this.dbEntity.Set<Stocks>().AsQueryable();
        }

        /// <summary>
        /// Inserts a new Stock into the table
        /// </summary>
        /// <param name="entity">Inserted entity</param>
        public void Insert(Stocks entity)
        {
            this.dbEntity.Stocks.Add(entity);
            try
            {
                this.dbEntity.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (e.InnerException != null)
                {
                    if (e.InnerException.InnerException != null)
                    {
                        Console.WriteLine(e.InnerException.InnerException.Message);
                        Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                        while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                        {
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Remove a Stock from the table
        /// </summary>
        /// <param name="entity">Removed Stocks</param>
        public void Remove(Stocks entity)
        {
            this.dbEntity.Stocks.Remove(entity);
            this.dbEntity.SaveChanges();
        }
    }
}
